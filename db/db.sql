create table product( 
    id integer primary key auto_increment,
    title varchar(100),
    description varchar(300),
    price float,
    categoryId integer
);

