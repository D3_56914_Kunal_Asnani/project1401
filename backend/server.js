const express =require('express')
const cors =require('cors')
const routerProduct= require('./routes/product')


const app =express()
app.use(cors('*'))
app.use(express.json())

app.use('/product',routerProduct)



app.listen(4000,'0.0.0.0',()=>{

    console.log('server started at port 4000')

})

