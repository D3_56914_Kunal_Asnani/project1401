
const { response } = require('express')
const express =require('express')
const db =require('../db')
const utils =require('../utils')


const router =express.Router()


router.get('/',(request,response)=>{

    const statement =`select id,title,price,Description,categoryId from product`
    
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))

    })
    

})
router.post('/',(request,response)=>{
    const{title,description,price,categoryid}= request.body
  
    const statement =`insert into product(title,description,price,categoryId) values
    ('${title}','${description}',${price},${categoryid})`

    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id',(request,response)=>{

const{id}= request.params
const{title,description,price,categoryid}=request.body

const statement=`update product 
set
title ='${title}',description='${description}',price=${price},categoryId=${categoryid} where id=${id}`

db.execute(statement,(error,result)=>{
    response.send(utils.createResult(error,result))

})
})

router.delete('/:id',(request,response)=>{

    const{id}=request.params
 const statement =`delete from product where id=${id}`

 db.execute(statement,(error,result)=>{
    response.send(utils.createResult(error,result))

 })

})


module.exports = router